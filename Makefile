all: root_shield.img root_tn7.img

ramfs.img.gz:
	find . ! -iname README.md ! -iname LICENSE ! -iname Makefile ! -iname "zImage*" ! -iname "*.dtb" ! -iname "*.img" ! -iname $@ |grep -v "\\.git*" |cpio -o --format=newc |gzip -c - > $@

zImage_roth: zImage tegra114-roth.dtb
	cat $^ >$@

root_shield.img: zImage_roth ramfs.img.gz
	mkbootimg --kernel $< --ramdisk ramfs.img.gz -o $@

zImage_tn7: zImage tegra114-tn7.dtb
	cat $^ >$@

zImage_st8_3.10: zImage tegra124-tn8-p1761-1270-a04-e-battery.dtb
	cat $^ >$@

zImage_st8: zImage tegra124-st8.dtb
	cat $^ >$@

root_tn7.img: zImage_tn7 ramfs.img.gz
	mkbootimg --kernel $< --ramdisk ramfs.img.gz -o $@

root_st8_3.10.img: zImage_st8_3.10 ramfs.img.gz
	mkbootimg --kernel $< --ramdisk ramfs.img.gz \
	--cmdline "tegraid=40.1.1.0.0 mem=1820M@2048M memtype=0 vpr=186M@3910M \
	tsec=32M@3878M otf_key=28cd8d41c01b1234f21f76ec9f133332 tzram=4M@3872M \
	ddr_die=2048M@2048M section=256M \
	video=tegrafb no_console_suspend=1 console=tty1 \
	tegra_fbmem=18743296@0xad012000 \
	watchdog=disable debug" \
		   -o $@
#	core_edp_mv=1150 core_edp_ma=4000 pmuboard=0x06e1:0x00ea:0x04:0x00:0x00 \
#	displayboard=0x0791:0x03e8:0x00:0x00:0x00 power_supply=Battery \
# gpt_sector=41983 modem_id=0 watchdog=disable nck=1048576@0xf1d00000" \
#	board_info=0x06e1:0x00ea:0x04:0x00:0x00 tegraboot=sdmmc gpt \


root_st8.img: zImage_st8 ramfs.img.gz
	mkbootimg --kernel $< --ramdisk ramfs.img.gz \
		   -o $@

l4t_tn8.img: zImage_tn8 l4t.img.gz
	mkbootimg --kernel $< --ramdisk l4t.img.gz \
	--cmdline "tegraid=40.1.1.0.0 vmalloc=256M video=tegrafb console=tty1 earlyprintk" \
	-o $@


clean:
	rm -f root_*.img zImage_* ramfs.img.gz l4t_*.img
